import request from '@/utils/request'
import {
    baseUrl
} from "@/config/env";

// 开始任务
export function startTask(order_id) {
    return request({
        url: `${baseUrl}/users/startmaketask/`,
        method: 'get',
        params: {
            order_id
        }
    })
};

// 保存
export function saveItem(
    task_id,
    num,
    data) {
    return request({
        url: `${baseUrl}/users/savetask/`,
        method: 'post',
        data: {
            task_id,
            num,
            data
        }
    })
}

// 上一条
export function lastItem(
    new_index,
    task_id,
    num,
    data) {
    return request({
        url: `${baseUrl}/users/lastonetask/`,
        method: 'post',
        data: {
            new_index,
            task_id,
            num,
            data
        }
    })
}

// 下一条
export function nextItem(
    new_index,
    task_id,
    num,
    data) {
    return request({
        url: `${baseUrl}/users/nextonetask/`,
        method: 'post',
        data: {
            new_index,
            task_id,
            num,
            data
        }
    })
}

// 查看任务
export function queryTask(task_id) {
    return request({
        url: `${baseUrl}/users/querytask/`,
        method: 'get',
        params: {
            task_id
        }
    })
};

// 查看任务 -- 保存
export function saveItemCheck(
    task_id,
    num,
    data) {
    return request({
        url: `${baseUrl}/users/savenowtask/`,
        method: 'post',
        data: {
            task_id,
            num,
            data
        }
    })
};

// 进入审核界面
export function beginAudit(order_id, check_spot) {
    return request({
        url: `${baseUrl}/audit/choose/`,
        method: 'get',
        params: {
            order_id,
            check_spot
        }
    })
};

// 审核 上一条 下一条
export function lastAndNext(index, order_id) {
    return request({
        url: `${baseUrl}/audit/last_next/`,
        method: 'get',
        params: {
            index,
            order_id
        }
    })
}

export function passItem(
    order_id,
    task_id,
    num,
    data) {
    return request({
        url: `${baseUrl}/audit/hege_save/`,
        method: 'post',
        data: {
            order_id,
            task_id,
            num,
            data
        }
    })
};

export function notPassItem(
    order_id,
    task_id,
    comment
    ) {
    return request({
        url: `${baseUrl}/audit/reject/`,
        method: 'post',
        data: {
            order_id,
            task_id,
            comment
        }
    })
};

export function recoverItem(
    order_id,
    task_id
) {
    return request({
        url: `${baseUrl}/audit/recovery/`,
        method: 'post',
        data: {
            order_id,
            task_id
        }
    })
};

export function passAllItem(
    order_id
) {
    return request({
        url: `${baseUrl}/audit/allpass/`,
        method: 'post',
        data: {
            order_id
        }
    })
};

export function notPassAllItem(
    order_id
) {
    return request({
        url: `${baseUrl}/audit/allnotpass/`,
        method: 'post',
        data: {
            order_id
        }
    })
};




