import request from '@/utils/request'
import {
    baseUrl
} from "@/config/env";

// 所有项目
export function getAllTask(page, page_size) {
    return request({
        url: `${baseUrl}/bidding/projectlist/`,
        method: 'get',
        params: {
            page,
            page_size
        }
    })
};

// 标注员审核员列表
export function labelTest(project_id) {
    return request({
        url: `${baseUrl}/bidding/testmarkguildmember/`,
        method: 'get',
        params: {
            project_id
        }
    })
};

// 提交试标
export function subTest(project_id, mark_id, audit_id) {
    return request({
        url: `${baseUrl}/bidding/testmarkdistribution/`,
        method: 'post',
        data: {
            project_id,
            mark_id,
            audit_id
        }
    })
};

// 提交报价
export function subPrice(project_id, price, finish_time) {
    return request({
        url: `${baseUrl}/bidding/startbidding/`,
        method: 'post',
        data: {
            project_id,
            price,
            finish_time
        }
    })
};

// 正在进行
export function getOngoingTask(page, page_size) {
    return request({
        url: `${baseUrl}/bidding/underwayprojects/`,
        method: 'get',
        params: {
            page,
            page_size
        }
    })
};

// 已完成
export function getFinishedTask(page, page_size) {
    return request({
        url: `${baseUrl}/bidding/biddingfinishedlist/`,
        method: 'get',
        params: {
            page,
            page_size
        }
    })
};

// 失败
export function getFailTask(page, page_size) {
    return request({
        url: `${baseUrl}/bidding/biddingshibailist/`,
        method: 'get',
        params: {
            page,
            page_size
        }
    })
};

// 提交日志
export function subLog(bidding_id, join, output, journal, finish_time) {
    return request({
        url: `${baseUrl}/bidding/report/`,
        method: 'post',
        data: {
            bidding_id,
            join,
            output,
            journal,
            finish_time
        }
    })
};

// 查看日志
export function getLogList(bidding_id) {
    return request({
        url: `${baseUrl}/bidding/viewreports/`,
        method: 'get',
        params: {
            bidding_id
        }
    })
};

// 提交项目
export function submitProject(bidding_id) {
    return request({
        url: `${baseUrl}/bidding/submitproject/`,
        method: 'post',
        data: {
            bidding_id
        }
    })
};