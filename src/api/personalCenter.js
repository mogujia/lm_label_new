import request from '@/utils/request'
import {
    baseUrl
} from "@/config/env";

// 个人中心任务列表 正在进行
export function getTaskListUnderway(page, page_size) {
    return request({
        url: `${baseUrl}/users/underwaylist/`,
        method: 'get',
        params: {
            page,
            page_size
        }
    })
};

// 个人中心任务列表 已完成
export function getTaskListFinished(page, page_size) {
    return request({
        url: `${baseUrl}/users/completedtasklist/`,
        method: 'get',
        params: {
            page,
            page_size
        }
    })
};

// 公告
export function getNoticeList() {
    return request({
        url: `${baseUrl}/users/noticeview/`,
        method: 'get'
    })
};

// 查看任务
export function checkTaskList(order_id, start_time, end_time) {
    return request({
        url: `${baseUrl}/users/viewtasklist/`,
        method: 'get',
        params: {
            order_id,
            start_time,
            end_time
        }
    })
};

// 审核任务列表
export function getAuditList(page, page_size, batch_id) {
    return request({
        url: `${baseUrl}/audit/info/`,
        method: 'get',
        params: {
            page, 
            page_size,
            batch_id
        }
    })
};

// 未通过任务
export function getNotpassList(order_id) {
    return request({
        url: `${baseUrl}/users/querynopasslist/`,
        method: 'get',
        params: {
            order_id
        }
    })
};