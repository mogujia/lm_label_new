import request from '@/utils/request'
import { baseUrl } from "@/config/env";

export function login(username, password) {
  return request({
    url: `${baseUrl}/users/authorizations/`,
    method: 'post',
    data: {
      username,
      password
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

export function regist(
  username,
  mobile,
  password,
  password2,
  imge_code,
  value,
  sms_code) {
  return request({
    url: `${baseUrl}/users/zhuce/`,
    method: 'post',
    data: {
      username,
      mobile,
      password,
      password2,
      imge_code,
      value,
      sms_code
    }
  })
}

export function getSmscode(mobile) {
  return request({
    url: `${baseUrl}/verifications/sms_code/${mobile}/`,
    method: 'get'
  })
}


