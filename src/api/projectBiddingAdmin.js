import request from '@/utils/request'
import {
    baseUrl
} from "@/config/env";

// 二审员列表
export function getTwoAudit() {
    return request({
        url: `${baseUrl}/bidding/viewtwoauditlist/`,
        method: 'get'
    })
};

export function subTwoAudit(project_id, two_audit_id) {
    return request({
        url: `${baseUrl}/bidding/zhidingershenyuan/`,
        method: 'post',
        data: {
            project_id,
            two_audit_id
        }
    })
};

// 竞标审核列表
export function getAuditBiddingList(project_id, page, page_size) {
    return request({
        url: `${baseUrl}/bidding/auditbiddinglist/`,
        method: 'get',
        params: {
            project_id,
            page,
            page_size
        }
    })
};

// 通过竞标审核
export function passAudit(bidding_id, batch_id, two_audit_array) {
    return request({
        url: `${baseUrl}/bidding/auditpass/`,
        method: 'post',
        data: {
            bidding_id,
            batch_id,
            two_audit_array
        }
    })
};

// 不通过竞标审核
export function notPassAudit(bidding_id, reason) {
    return request({
        url: `${baseUrl}/bidding/auditnotpass/`,
        method: 'post',
        data: {
            bidding_id,
            reason
        }
    })
};

// 项目管理列表
export function getProjectManageList(project_id, page, page_size) {
    return request({
        url: `${baseUrl}/bidding/projectgradelist/`,
        method: 'get',
        params: {
            project_id,
            page,
            page_size
        }
    })
};

// 日志反馈信息提交
export function subFeedback(report_id, comment) {
    return request({
        url: `${baseUrl}/bidding/fankuirizisave/`,
        method: 'post',
        data: {
            report_id,
            comment
        }
    })
};

// 评级分数提交
export function subGradeScore(bidding_id, grade_score) {
    return request({
        url: `${baseUrl}/bidding/projectgrade/`,
        method: 'post',
        data: {
            bidding_id,
            grade_score
        }
    })
};

// 关闭项目
export function closeProject(project_id) {
    return request({
        url: `${baseUrl}/bidding/closeproject/`,
        method: 'get',
        params: {
            project_id
        }
    })
};