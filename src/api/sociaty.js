import request from '@/utils/request'
import {
    baseUrl
} from "@/config/env";

// 公会用户身份判断
export function getMessage() {
    return request({
        url: `${baseUrl}/guilds/judge/`,
        method: 'get'
    })
};

export function createSociaty({
    name,
    urgent_phone,
    qq_group,
    description,
    create_reason}) {
    return request({
        url: `${baseUrl}/guilds/create_guild/`,
        method: 'post',
        data: {
            name,
            urgent_phone,
            qq_group,
            description,
            create_reason
        }
    })
};

// 所有公会列表
export function getSociatyList(page, page_size) {
    return request({
        url: `${baseUrl}/guilds/guild_list/`,
        method: 'get',
        params: {
            page,
            page_size
        }
    })
};

// 公会首页
export function getSociatyInfo() {
    return request({
        url: `${baseUrl}/guilds/guild_index/`,
        method: 'get'
    })
};

// 公会任务列表 正在进行
export function getListUnderway(page, page_size) {
    return request({
        url: `${baseUrl}/guilds/guildtaskunderway/`,
        method: 'get',
        params: {
            page,
            page_size
        }
    })
};

// 公会任务列表 已完成
export function getListFinished(page, page_size) {
    return request({
        url: `${baseUrl}/guilds/guildtaskfinished/`,
        method: 'get',
        params: {
            page,
            page_size
        }
    })
}

// 数量查看
export function checkCount(b_id) {
    return request({
        url: `${baseUrl}/guilds/guildtaskcount/`,
        method: 'get',
        params: { b_id }
    })
};

// 领取任务
export function getSociatyTask(batch_id, label) {
    return request({
        url: `${baseUrl}/tasks/mark/`,
        method: 'post',
        data: {
            batch_id,
            label
        }
    })
};

// 撤回申请入会
export function withdraw() {
    return request({
        url: `${baseUrl}/guilds/takebacksq/`,
        method: 'get'
    })
};

// 公告
export function getNoticeList() {
    return request({
        url: `${baseUrl}/guilds/member_notice_title/`,
        method: 'get'
    })
};

export function publishNotice(title, content) {
    return request({
        url: `${baseUrl}/guilds/release_summary/`,
        method: 'post',
        data: {
            title,
            content
        }
    })
}

export function deleteNotice(id) {
    return request({
        url: `${baseUrl}/guilds/remove_summary/`,
        method: 'get',
        params: {
            id
        }
    })
}