export const types = {
    '1': '标注',
    '2': '审核',
    '3': '二审',
    '4': '试标',
    '5': '采集'
}

export const batchAttr = {
    '1': '个人',
    '2': '公会'
}

export const categories = {
    '1': '数据标注',
    '2': '数据采集',
    '3': '方案征集',
    '4': '其他'
}

export const page_category_route = {
    '1': 'audio_ASR',
}
