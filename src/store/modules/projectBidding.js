import {
    getAllTask,
    getOngoingTask,
    getFailTask,
    getFinishedTask,
    getLogList
}
from '@/api/projectBidding'

let projectBidding = {
    state: {
        page: 1,
        page_size: 9,
        total: 0,
        allTaskList: [],
        ongoingTaskList: [],
        failTaskList: [],
        finishedTaskList: [],
        logList: []
    },
    mutations: {
        SET_PAGE: (state, page) => {
            state.page = page
        },
        SET_PAGE_SIZE: (state, page_size) => {
            state.page_size = page_size
        },
        SET_TOTAL: (state, total) => {
            state.total = total
        },
        SET_ALLTASK_lIST: (state, allTaskList) => {
            state.allTaskList = allTaskList
        },
        SET_ONGOINGTASK_LIST: (state, ongoingTaskList) => {
            state.ongoingTaskList = ongoingTaskList
        }, 
        SET_FAILTASK_LIST: (state, failTaskList) => {
            state.failTaskList = failTaskList
        },
        SET_FINISHTASK_LIST: (state, finishedTaskList) => {
            state.finishedTaskList = finishedTaskList
        },
        SET_LOG_LIST: (state, logList) => {
            state.logList = logList
        },
    },
    actions: {
        GetAllTask({
                commit,
                state
            }) {
            return new Promise((resolve, reject) => {
                getAllTask(state.page, state.page_size).then(response => {
                    commit('SET_TOTAL', response.count);
                    commit('SET_ALLTASK_lIST', response.results);
                    resolve(response)
                }).catch(error => {
                    reject(error)
                });
            })
        }, 
        GetOngoingTask({ commit, state }) {
            return new Promise((resolve, reject) => {
                getOngoingTask(state.page, state.page_size).then(response => {
                    commit('SET_TOTAL', response.count);
                    commit('SET_ONGOINGTASK_LIST', response.results);
                    resolve(response)
                }).catch(error => {
                    reject(error)
                });
            })
        },
        GetFailTask({
            commit,
            state
        }) {
            return new Promise((resolve, reject) => {
                getFailTask(state.page, state.page_size).then(response => {
                    commit('SET_TOTAL', response.count);
                    commit('SET_FAILTASK_LIST', response.results);
                    resolve(response)
                }).catch(error => {
                    reject(error)
                });
            })
        }, 
        GetFinishedTask({
            commit,
            state
        }) {
            return new Promise((resolve, reject) => {
                getFinishedTask(state.page, state.page_size).then(response => {
                    commit('SET_TOTAL', response.count);
                    commit('SET_FINISHTASK_LIST', response.results);
                    resolve(response)
                }).catch(error => {
                    reject(error)
                });
            })
        },
        GetLogList({
            commit
        }, bidding_id) {
            return new Promise((resolve, reject) => {
                getLogList(bidding_id).then(response => {
                    commit('SET_LOG_LIST', response.results);
                    resolve(response)
                }).catch(error => {
                    reject(error)
                });
            })
        }
    }
};

export default projectBidding;