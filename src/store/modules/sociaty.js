import {
    getMessage,
    getListUnderway,
    getListFinished,
    checkCount
}
from '@/api/sociaty'

const sociaty = {
    state: {
        status: '',   // 用户身份类型
        underwayList: [],
        finishList: [],
        sPage: 1,
        sPage_size: 9,
        sTotal: 0,
    },

    mutations: {
        SET_SPAGE: (state, sPage) => {
            state.sPage = sPage
        },
        SET_SPAGE_SIZE: (state, sPage_size) => {
            state.sPage_size = sPage_size
        },
        SET_STOTAL: (state, sTotal) => {
            state.sTotal = sTotal
        },
        SET_STATUS: (state, status) => {
            state.status = status
        },
        SET_UNDERWAY: (state, underwayList) => {
            state.underwayList = underwayList
        },
        SET_FINISH: (state, finishList) => {
            state.finishList = finishList
        }
    },

    actions: {
        GetMessage({
            commit
        }) {
            return new Promise((resolve, reject) => {
                getMessage().then(response => {
                    commit('SET_STATUS', response.user_status);
                    resolve(response)
                }).catch(error => {
                    reject(error)
                });
            })
        },

        GetListUnderway({
                commit, state
            }) {
            return new Promise((resolve, reject) => {
            getListUnderway(state.sPage, state.sPage_size).then((response) => {
                commit('SET_STOTAL', response.count);
                commit('SET_UNDERWAY', response.results);
                resolve(response)
            }).catch(error => {
                reject(error)
            })
            })
        },

        GetListFinished({
            commit, state
        }) {
            return new Promise((resolve, reject) => {
                getListFinished(state.sPage, state.sPage_size).then((response) => {
                    commit('SET_STOTAL', response.count);
                    commit('SET_FINISH', response.results);
                    resolve(response)
                }).catch(error => {
                    reject(error)
                })
            })
        },
        CheckCount({
            commit
        }, batchId) {
            return new Promise((resolve, reject) => {
                checkCount(batchId).then((response) => {
                    // commit('SET_STOTAL', response.count);
                    resolve(response)
                }).catch(error => {
                    reject(error)
                })
            })
        },

    }
};

export default sociaty