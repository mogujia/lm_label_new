import {
    startTask,
    saveItem,
    lastItem,
    nextItem,
    queryTask,
    saveItemCheck,
    beginAudit,
    lastAndNext,
    passItem
}
from '@/api/workSpaceLabel'

const label = {
    state: {
        index: 0,
        norm: '',   // 任务规范
        taskId: '', // 任务id
        fileName: '',
        taskfile: '',
        comment: '',
        // raw_data: {},   // 预处理结果
        data: {},
        num: 0     //总时长/总框数
    },
    mutations: {
        SET_TASKID: (state, taskId) => {
            state.taskId = taskId
        },
        SET_FILENAME: (state, fileName) => {
            state.fileName = fileName
        },
        SET_TASKFILE: (state, taskfile) => {
            state.taskfile = taskfile
        },
        SET_COMMENT: (state, comment) => {
            state.comment = comment
        },
        SET_NEXT_ITEM: (state) => {
            state.index += 1
        },
        SET_PREV_ITEM: (state) => {
            state.index--
        },
        // SET_RAW_DATA: (state, raw_data) => {
        //     state.raw_data = raw_data
        // },
        SET_DATA: (state, data) => {
            state.data = data       
        },
        SET_NUM: (state, num) => {
            state.num = num
        }
    },
    actions: {
        StartTask({ commit }, order_id) {
            return new Promise((resolve, reject) => {
                startTask(order_id).then(response => {
                    commit('SET_TASKID', response.id);
                    commit('SET_FILENAME', response.file);
                    commit('SET_TASKFILE', response.taskfile);
                    // commit('SET_RAW_DATA', response.raw_data);
                    commit('SET_DATA', response.data);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        SaveItem({
            state
        }) {
            return new Promise((resolve, reject) => {
                saveItem(state.taskId, state.num, state.data).then(response => {
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        }, 
        LastItem({
            commit,
            state
        }) {
            commit('SET_PREV_ITEM');
            return new Promise((resolve, reject) => {
                lastItem(state.index, state.taskId, state.num, state.data).then(response => {
                    commit('SET_TASKID', response.id);
                    commit('SET_FILENAME', response.file);
                    commit('SET_TASKFILE', response.taskfile);
                    commit('SET_DATA', response.data);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        NextItem({
            commit,
            state
        }) {
            commit('SET_NEXT_ITEM');
            return new Promise((resolve, reject) => {
                nextItem(state.index, state.taskId, state.num, state.data).then(response => {
                    commit('SET_TASKID', response.id);
                    commit('SET_FILENAME', response.file);
                    commit('SET_TASKFILE', response.taskfile);
                    commit('SET_DATA', response.data);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        QueryTask({
            commit
        }, task_id) {
            return new Promise((resolve, reject) => {
                queryTask(task_id).then(response => {
                    commit('SET_TASKID', response.id);
                    commit('SET_FILENAME', response.file);
                    commit('SET_TASKFILE', response.taskfile);
                    commit('SET_COMMENT', response.comment);
                    commit('SET_DATA', response.data);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        SaveItemCheck({
            state
        }) {
            return new Promise((resolve, reject) => {
                saveItemCheck(state.taskId, state.num, state.data).then(response => {
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        BeginAudit({
            commit
        }, {order_id, check_spot}) {
            return new Promise((resolve, reject) => {
                beginAudit(order_id, check_spot).then(response => {
                    commit('SET_TASKID', response.id);
                    commit('SET_FILENAME', response.file);
                    commit('SET_TASKFILE', response.taskfile);
                    commit('SET_DATA', response.data);
                    commit('SET_COMMENT', response.comment);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        // 审核 上一条 下一条
        LastAndNext({
            commit,
            state
        }, order_id) {
            return new Promise((resolve, reject) => {
                lastAndNext(state.index, order_id, state.num, state.data).then(response => {
                    commit('SET_TASKID', response.id);
                    commit('SET_FILENAME', response.file);
                    commit('SET_TASKFILE', response.taskfile);
                    commit('SET_DATA', response.data);
                    commit('SET_COMMENT', response.comment);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        }, 
        PassItem({
            state
        }, order_id) {
            return new Promise((resolve, reject) => {
                passItem(order_id, state.taskId, state.num, state.data).then(response => {
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },
    }
}

export default label;