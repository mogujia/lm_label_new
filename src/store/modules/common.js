import website from '@/const/website'
import identities from '@/const/identity'
const common = {

  state: {
    website: website,
    identities: identities
  },
  actions: {
  },
  mutations: {

  }
}
export default common
