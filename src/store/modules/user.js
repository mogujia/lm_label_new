import {
  login,
  logout,
  getInfo,
  regist,
  getSmscode
} from '@/api/login'
import {
  getToken,
  setToken,
  removeToken
} from '@/utils/auth'
import {
  setStore,
  getStore,
  removeStore
} from '@/utils/store'
const user = {
  state: {
    token: getToken(),
    name: getStore({
      name: 'name'
    }) || '' , //用户名
    avatar: getStore({
      name: 'avatar'
    }) || '', //头像
    roles: getStore({
      name: 'roles'
    }) || [], // 权限身份验证
    role: getStore({
      name: 'role'
    }) || '', //用户角色
    identity: getStore({
      name: 'identity'
    }) || '', //用户身份
    isLock: getStore({
      name: 'isLock'
    }) || false,
    lockPasswd: getStore({
      name: 'lockPasswd'
    }) || '',
    browserHeaderTitle: getStore({
      name: 'browserHeaderTitle'
    }) || '数米任务平台'
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
      setStore({
        name: 'name',
        content: state.name
      })
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
      setStore({
        name: 'avatar',
        content: state.avatar
      })
    },
    SET_ROLES: (state) => {
      state.roles.push(state.identity)
      setStore({
        name: 'roles',
        content: state.roles
      })
    },
    CLEAR_ROLES: (state) => {
      state.roles = [];
      removeStore({
        name: 'roles'
      })
    },
    SET_IDENTITY: (state, identity) => {
      state.identity = identity
      setStore({
        name: 'identity',
        content: state.identity
      })
    },
    SET_ROLE: (state, role) => {
      state.role = role
      setStore({
        name: 'role',
        content: state.role
      })
    },
    SET_LOCK_PASSWD: (state, lockPasswd) => {
      state.lockPasswd = lockPasswd
      setStore({
        name: 'lockPasswd',
        content: state.lockPasswd,
        type: 'session'
      })
    },
    SET_LOCK: (state, action) => {
      state.isLock = true
      setStore({
        name: 'isLock',
        content: state.isLock,
        type: 'session'
      })
    },
    CLEAR_LOCK: (state, action) => {
      state.isLock = false
      state.lockPasswd = ''
      removeStore({
        name: 'lockPasswd'
      })
      removeStore({
        name: 'isLock'
      })
    },
    SET_BROWSERHEADERTITLE: (state, action) => {
      state.browserHeaderTitle = action.browserHeaderTitle
    }

  },

  actions: {
    // 登录
    Login({
      commit
    }, userInfo) {
      const username = userInfo.username.trim();
      return new Promise((resolve, reject) => {
        login(username, userInfo.password).then(response => {
          const data = response;
          setToken(data.token);
          commit('SET_TOKEN', data.token)
          commit('SET_NAME', data.username)
          commit('SET_AVATAR', data.userpic)
          commit('SET_ROLE', data.role)
          commit('SET_IDENTITY', data.identity)
          commit('SET_ROLES')
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 注册
    Regist({
      commit
    }, userInfo) {
      const username = userInfo.username;
      const mobile = userInfo.mobile;
      const password = userInfo.password;
      const password2 = userInfo.password2;
      const imge_code = userInfo.imge_code;
      const value = userInfo.value;
      const sms_code = userInfo.sms_code;
      return new Promise((resolve, reject) => {
        regist(username, mobile, password, password2, imge_code, value, sms_code).then(response => {
          resolve(response);
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取短信验证码
    GetSmscode({
      commit
    }, mobile) {
      return new Promise((resolve, reject) => {
        getSmscode(mobile).then(response => {
          resolve(response);
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息  
    GetInfo({
      commit,
      state
    }) {
      return new Promise((resolve, reject) => {
        getInfo(state.token).then(response => {
          const data = response
          if (data.roles && data.roles.length > 0) { // 验证返回的roles是否是一个非空数组
            commit('SET_ROLES', data.roles)
          } else {
            reject('getInfo: roles must be a non-null array !')
          }
          commit('SET_NAME', data.name)
          commit('SET_AVATAR', data.avatar)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    LogOut({
      commit,
      state
    }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('CLEAR_ROLES')
          commit('CLEAR_LOCK')
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({
      commit
    }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        commit('CLEAR_ROLES')
        removeToken()
        resolve()
      })
    },
    // 动态修改权限
    ChangeRoles({
      commit
    }, role) {
      return new Promise(resolve => {
        commit('SET_TOKEN', role)
        setToken(role)
        getInfo(role).then(response => {
          const data = response
          commit('SET_ROLES', data.roles)
          commit('SET_NAME', data.name)
          commit('SET_AVATAR', data.avatar)
          resolve()
        })
      })
    }
  }
}

export default user