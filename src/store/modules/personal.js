import {
    getTaskListUnderway,
    getTaskListFinished,
    checkTaskList,
    getAuditList,
    getNotpassList
}
from '@/api/personalCenter'

const personalCenter = {
    state: {
        page: 1,
        page_size: 9,
        total: 0,
        underwayList: [],
        finishList: [],
        auditList: [],
        task_id_list: [],   //订单下所以的任务id
    },

    mutations: {
        SET_PAGE: (state, page) => {
            state.page = page
        },
        SET_PAGE_SIZE: (state, page_size) => {
            state.page_size = page_size
        },
        SET_TOTAL: (state, total) => {
            state.total = total
        },
        SET_UNDERWAY_LIST: (state, underwayList) => {
            state.underwayList = underwayList
        },
        SET_FINISH_LIST: (state, finishList) => {
            state.finishList = finishList
        },
        SET_AUDIT_LIST: (state, auditList) => {
            state.auditList = auditList
        },
        SET_TASKID_LIST: (state, task_id_list) => {
            state.task_id_list = task_id_list
        }
    },

    actions: {
        GetTaskListUnderway({
            commit,
            state
        }) {
            return new Promise((resolve, reject) => {
                getTaskListUnderway(state.page, state.page_size).then((response) => {
                    commit('SET_TOTAL', response.count);
                    commit('SET_UNDERWAY_LIST', response.results);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                })
            })
        },
        GetTaskListFinished({
            commit,
            state
        }) {
            return new Promise((resolve, reject) => {
                getTaskListFinished(state.page, state.page_size).then((response) => {
                    commit('SET_TOTAL', response.count);
                    commit('SET_FINISH_LIST', response.results);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                })
            })
        },
        CheckTaskList({
            commit
        }, {
            order_id, start_time, end_time
        }) {
            return new Promise((resolve, reject) => {
                checkTaskList(order_id, start_time, end_time).then((response) => {
                    commit('SET_TASKID_LIST', response.task_id_list);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                })
            })
        }, 
        GetAuditList({
            commit,
            state
        }, batch_id) {
            return new Promise((resolve, reject) => {
                getAuditList(state.page, state.page_size, batch_id).then((response) => {
                    commit('SET_TOTAL', response.count);
                    commit('SET_AUDIT_LIST', response.results);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                })
            })
        },
        GetNotpassList({
            commit
        }, order_id) {
            return new Promise((resolve, reject) => {
                getNotpassList(order_id).then(response => {
                    commit('SET_TASKID_LIST', response.task_id_list);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },
    }
};

export default personalCenter