import {
    getAuditBiddingList,
    getProjectManageList
}
from '@/api/projectBiddingAdmin'

let projectBiddingAdmin = {
    state: {
        page: 1,
        page_size: 9,
        total: 0,
        auditBiddingList: [],
        projectManageList: []
    },
    mutations: {
        SET_PAGE: (state, page) => {
            state.page = page
        },
        SET_PAGE_SIZE: (state, page_size) => {
            state.page_size = page_size
        },
        SET_TOTAL: (state, total) => {
            state.total = total
        },
        SET_AUDITBIDDING_LIST: (state, auditBiddingList) => {
            state.auditBiddingList = auditBiddingList
        },
        SET_PROMANAGE_LIST: (state, projectManageList) => {
            state.projectManageList = projectManageList
        },
    },
    actions: {
        GetAuditBiddingList({
            commit,
            state
        }, project_id) {
            return new Promise((resolve, reject) => {
                getAuditBiddingList(project_id, state.page, state.page_size).then(response => {
                    commit('SET_TOTAL', response.count);
                    commit('SET_AUDITBIDDING_LIST', response.results);
                    resolve(response)
                }).catch(error => {
                    reject(error)
                });
            })
        },
        GetProjectManageList({
            commit,
            state
        }, project_id) {
            return new Promise((resolve, reject) => {
                getProjectManageList(project_id, state.page, state.page_size).then(response => {
                    commit('SET_TOTAL', response.count);
                    commit('SET_PROMANAGE_LIST', response.results);
                    resolve(response)
                }).catch(error => {
                    reject(error)
                });
            })
        },
    }
};

export default projectBiddingAdmin;