import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import common from './modules/common'
import getters from './getters'
import fullScreen from './modules/fullScreen'
import permission from './modules/permission'
import tagsView from './modules/tagsView'
import errorLog from './modules/errorLog'
import sociaty from './modules/sociaty'
import personalCenter from './modules/personal'
import projectBidding from './modules/projectBidding'
import projectBiddingAdmin from './modules/projectBiddingAdmin'
import label from './modules/workSpaceLabel'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    user,
    common,
    fullScreen,
    permission,
    tagsView,
    errorLog,
    sociaty,
    personalCenter,
    projectBidding,
    projectBiddingAdmin,
    label
  },
  getters
})

export default store
