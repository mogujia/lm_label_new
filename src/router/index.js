import Vue from 'vue'
import Router from 'vue-router'

// const _import = require('./_import_' + process.env.NODE_ENV)
// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
export const constantRouterMap = [
  // {
  //   path: '/',
  //   component: Layout,
  //   redirect: '/login',
  //   name: 'login',
  //   hidden: true
  // },
  {
    path: "/",
    component: Layout,
    redirect: "/dashboard/dashboard",
  },
  {
    path: '*',
    redirect: '/'
  },
  {
    path: "/login",
    component: () => import("@/views/login"),
    name: "登录",
    hidden: true,
  },
  {
    path: "/forget",
    component: () => import("@/views/forget"),
    name: "忘记密码",
    hidden: true,
  },
  {
    path: "/register",
    component: () => import("@/views/register"),
    name: "注册",
    hidden: true,
  },
  {
    path: "/404",
    component: () => import("@/views/errorPage/404"),
    hidden: true,
  },
  {
    path: "/401",
    component: () => import("@/views/errorPage/401"),
    hidden: true,
  },
  // 锁屏
  {
    path: "/lock",
    hidden: true,
    name: "锁屏页",
    component: () => import("@/views/common/lock"),
  },
  // 个人中心
  {
    path: "/dashboard",
    component: Layout,
    redirect: "/dashboard/dashboard",
    name: "personalCenter",
    meta: {
      title: "个人中心",
      icon: "dashboard"
    },
    children: [{
        path: "dashboard",
        name: "myTask",
        component: () => import("@/views/personalCenter/myTask"),
        meta: {
          title: "我的任务",
          icon: "list"
        },
      },
      // {
      //   path: "myProfit",
      //   name: "myProfit",
      //   component: () => import("@/views/personalCenter/myProfit"),
      //   meta: {
      //     title: "我的收益",
      //     icon: "money"
      //   },
      // },
      // {
      //   path: "projectBidding ",
      //   name: "projectBidding",
      //   component: () => import("@/views/personalCenter/projectBidding"),
      //   meta: {
      //     title: "项目竞标",
      //     icon: "lock"
      //   },
      // },
      // {
      //   path: "/auditBidding/:id",
      //   hidden: true,
      //   name: "auditBidding",
      //   component: () => import("@/views/personalCenter/components/auditBidding"),
      //   meta: {
      //     title: "竞标审核"
      //   },
      // },
      // {
      //   path: "/projectManage/:id",
      //   hidden: true,
      //   name: "projectManage",
      //   component: () => import("@/views/personalCenter/components/projectManage"),
      //   meta: {
      //     title: "项目管理"
      //   },
      // },
    ],
  },
  // 我的公会
  // {
  //   path: "/sociaty",
  //   component: Layout,
  //   redirect: "/sociaty/sociatyInfo",
  //   name: "sociaty",
  //   meta: {
  //     title: "我的公会",
  //     icon: "people"
  //   },
  //   children: [
  //     {
  //       path: "sociatyInfo",
  //       name: "sociatyInfo",
  //       component: () => import("@/views/sociaty/sociatyInfo"),
  //       meta: {
  //         title: "公会信息",
  //         icon: "clipboard"
  //       }
  //     },
  //     {
  //       path: "sociatyTask",
  //       name: "sociatyTask",
  //       component: () => import("@/views/sociaty/sociatyTask"),
  //       meta: {
  //         title: "公会任务",
  //         icon: "list"
  //       },
  //     },
  //     {
  //       path: "sociatyMember",
  //       name: "sociatyMember",
  //       component: () => import("@/views/sociaty/sociatyMember"),
  //       meta: {
  //         title: "成员管理",
  //         icon: "peoples"
  //       },
  //     },
  //     {
  //       path: "memberInvite",
  //       name: "memberInvite",
  //       component: () => import("@/views/sociaty/memberInvite"),
  //       meta: {
  //         title: "会员招募",
  //         icon: "guide"
  //       },
  //     },
  //     // 加入公会
  //     {
  //       path: "/join",
  //       hidden: true,
  //       name: "join",
  //       component: () => import("@/views/sociaty/components/join"),
  //       meta: {
  //         title: "加入公会"
  //       },
  //     },
  //     // 创建公会
  //     {
  //       path: "/create",
  //       hidden: true,
  //       name: "create",
  //       component: () => import("@/views/sociaty/components/create"),
  //       meta: {
  //         title: "创建公会"
  //       },
  //     },
  //   ],
  // },
  // ASR语音标注界面
  {
    path: "/audioASR/:id",
    hidden: true,
    name: "audio_ASR",
    title: 'ASR语音标注',
    component: () => import("@/views/audioWorkPage/audioASR"),
  },
  // 第三方官网
  //  {
  //    path: "/myiframe",
  //    component: Layout,
  //    redirect: "/myiframe",
  //    children: [
  //      {
  //        path: ":routerPath",
  //        name: "myiframe",
  //        component: () => import("@/components/nx-iframe"),
  //        meta: {
  //          title: "搜索工具",
  //          icon: "baidumap"
  //        },
  //      },
  //    ],
  //  },

];

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap
})
// 权限路由
export const asyncRouterMap = [
  // 我的公会
  {
    path: "/sociaty",
    component: Layout,
    redirect: "/sociaty/sociatyInfo",
    name: "sociaty",
    meta: {
      title: "我的公会",
      icon: "people",
      roles: [0, 1, 2, 3, 4]
    },
    children: [{
        path: "sociatyInfo",
        name: "sociatyInfo",
        component: () => import("@/views/sociaty/sociatyInfo"),
        meta: {
          title: "公会信息",
          icon: "clipboard",
        }
      },
      {
        path: "sociatyTask",
        name: "sociatyTask",
        component: () => import("@/views/sociaty/sociatyTask"),
        meta: {
          title: "公会任务",
          icon: "list",
          roles: [1, 2, 3, 4]
        },
      },
      {
        path: "sociatyMember",
        name: "sociatyMember",
        component: () => import("@/views/sociaty/sociatyMember"),
        meta: {
          title: "成员管理",
          icon: "peoples",
          roles: [1, 3]
        },
      },
      {
        path: "memberInvite",
        name: "memberInvite",
        component: () => import("@/views/sociaty/memberInvite"),
        meta: {
          title: "会员招募",
          icon: "guide",
          roles: [1, 3]
        },
      },
      // 加入公会
      {
        path: "/join",
        hidden: true,
        name: "join",
        component: () => import("@/views/sociaty/components/join"),
        meta: {
          title: "加入公会"
        },
      },
      // 创建公会
      {
        path: "/create",
        hidden: true,
        name: "create",
        component: () => import("@/views/sociaty/components/create"),
        meta: {
          title: "创建公会"
        },
      },
    ],
  },
  {
    path: "/projectBidding",
    component: Layout,
    redirect: "/projectBidding/projectBidding",
    name: "projectBidding",
    meta: {
      title: "项目竞标",
      icon: "lock",
      roles: [3, 5]
    },
    children: [{
        path: "projectBidding",
        name: "项目竞标",
        component: () => import("@/views/personalCenter/projectBidding"),
        meta: {
          title: "项目竞标",
          icon: "lock"
        },
      },
      {
        path: "/auditBidding/:id",
        hidden: true,
        name: "auditBidding",
        component: () => import("@/views/personalCenter/components/auditBidding"),
        meta: {
          title: "竞标审核"
        },
      }, {
        path: "/projectManage/:id",
        hidden: true,
        name: "projectManage",
        component: () => import("@/views/personalCenter/components/projectManage"),
        meta: {
          title: "项目管理"
        },
      },
    ]
  },
  // {
  //       path: '/checkProgress',
  //       component: Layout,
  //       meta: {
  //         title: '进度查看',
  //         icon: 'example',
  //         roles: [6]
  //       },
  //       children: [
  //         {
  //           path: 'checkProgress',
  //           name: 'checkProgress',
  //           component: () => import('@/views/checkProgress')
  //         }
  //       ]
  //     },
]

// export default new Router({
//   // mode: 'history', //后端支持可开
//   scrollBehavior: () => ({ y: 0 }),
//   routes: constantRouterMap
// })
// // 权限路由
// export const asyncRouterMap = [
//   {
//     path: '/permission',
//     component: Layout,
//     redirect: '/permission/page',
//     name: 'permission',
//     alwaysShow: true, // will always show the root menu
//     meta: {
//       title: 'permission',
//       icon: 'lock',
//       roles: ['1', '3'] // you can set roles in root nav
//     },
//     children: [{
//       path: 'page',
//       component: () => import('@/views/permission/page'),
//       name: 'pagePermission',
//       meta: {
//         title: 'pagePermission',
//         roles: ['admin'] // or you can only set roles in sub nav
//       }
//     }, {
//       path: 'directive',
//       component: () => import('@/views/permission/directive'),
//       name: 'directivePermission',
//       meta: {
//         title: 'directivePermission'
//         // if do not set roles, means: this page does not require permission
//       }
//     }]
//   },
//   {
//     path: '/error',
//     component: Layout,
//     redirect: 'noredirect',
//     name: 'errorPages',
//     meta: {
//       title: 'errorPages',
//       icon: '404'
//     },
//     children: [
//       { path: '401', component: () => import('@/views/errorPage/401'), name: 'page401', meta: { title: 'page401', noCache: true }},
//       { path: '404', component: () => import('@/views/errorPage/404'), name: 'page404', meta: { title: 'page404', noCache: true }}
//     ]
//   },
//   // 错误日志
//   {
//     path: '/errorLog',
//     component: Layout,

//     children: [
//       {
//         path: 'errorLog',
//         name: 'errorLog',
//         component: () => import('@/views/errorLog/errorLog'),
//         meta: { title: 'Errorlog', icon: 'errorLog' }
//       }
//     ]
//   },
//   { path: '*', redirect: '/404', hidden: true }]